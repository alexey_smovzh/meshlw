package com.fasthamster.meshlwpro;

import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by alex on 31.12.16.
 */

public class Touchscreen extends InputAdapter {

    private Vector3 position = new Vector3();
    private Vector3 centerScreen = new Vector3();
    private int touchX, touchY;

    private final static float ALPHA = 0.04f;


    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        touchX = screenX;
        touchY = screenY;

        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {

        position.x = centerScreen.x - (float)(touchX - screenX);        // horizontal
        position.y = centerScreen.y + (float)(touchY - screenY);        // invert Y

        return false;
    }

    // Getter
    public Vector3 getPosition() {
        return position;
    }

    // Setter
    public void setInitialPosition(Vector3 p) {

        position.set(p);
        centerScreen.set(p);

    }

    public void update() {
        // fuzzy equality testing
        if(position.epsilonEquals(centerScreen, 1f) == false)
            position.interpolate(centerScreen, ALPHA, Interpolation.linear);

    }
}

