package com.fasthamster.meshlwpro;

/**
 * Created by alex on 07.12.16.
 */

public interface DeviceOrientation {

    boolean isAvailable();
    float[] getOrientation();
    void pause();
    void resume();

}
