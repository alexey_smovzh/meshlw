package com.fasthamster.meshlwpro;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

import java.util.Random;


public class MeshLW extends ApplicationAdapter {

	public static final String TAG = MeshLW.class.getName();
	public static final String PAID_PACKAGE = "com.fasthamster.meshlwpro";
	public static final Random rand = new Random();

	private static DeviceOrientation orientation;
	private com.fasthamster.meshlwpro.Touchscreen touchscreen;
	private MeshColors colors;
	private com.fasthamster.meshlwpro.Rotation rotation;
	private Matrix4 projection = new Matrix4();
	private ShaderProgram shader;
	private Mesh mesh;
	private Triangulator triangulator0;
	private Triangulator triangulator1;
	private Runnable runnable0;
	private Runnable runnable1;
	private Preferences preferences;

	private Vector2 resolution = new Vector2();
	private Vector3 newColor0, newColor1;
	private int colorMode;
	private Vector3 color0;
	private Vector3 color1;
	private float lastColorChangeTime = 0f;
	private boolean autoChangeColor;
	private float autoChangeColorIntv;
	private float lightZ;
	private boolean outlines;
	private int cellSize = CELL_SIZE_DEFAULT;
	private float lastMeshChangeTime = 0f;
	private boolean autoChangeMesh;
	private int autoChangeMeshIntv;
	private boolean gyroscopeEnabled;
	private Vector3 lightPosition = new Vector3();
	private boolean gyroscopeUVmove;
	private Vector2 uvMove = new Vector2();
	private boolean NPOTsupport;
	private boolean paused;
	private static boolean preferencesChanged;


	public static final int COLOR_MODE_COLOR = 0;
	public static final int COLOR_MODE_GRADIENT = 1;
	public static final int COLOR_MODE_RANDOM_COLOR = 2;
	public static final int COLOR_MODE_RANDOM_GRADIENT = 3;

	public static final String PREFS_NAME = "settings";
	public static final String LIGHT_Z_ALIAS = "lightZ";
	public static final int LIGHT_Z_DEFAULT = 2;
	public static final int LIGHT_Z_MIN = 1;
	public static final String COLOR_MODE_ALIAS = "colorMode";
	public static final int COLOR_MODE_DEFAULT = COLOR_MODE_RANDOM_COLOR;
	public static final String COLOR_0_ALIAS = "color0";
	public static final int COLOR_0_DEFAULT =  (0xFF << 24) | (89 << 16) | (45 << 8) | 140;
	public static final String COLOR_1_ALIAS = "color1";
	public static final int COLOR_1_DEFAULT =  (0xFF << 24) | (64 << 16) | (170 << 8) | 72;
	public static final String AUTO_CHANGE_COLOR_ALIAS = "autoChangeColor";
	public static final boolean AUTO_CHANGE_COLOR_DEFAULT = true;
	public static final String AUTO_CHANGE_COLOR_INTERVAL_ALIAS = "autoChangeColorIntv";
	public static final int AUTO_CHANGE_COLOR_INTERVAL_DEFAULT = 30;
	public static final int AUTO_CHANGE_COLOR_INTERVAL_MIN = 10;								// 10 seconds
	public static final String OUTLINES_ALIAS = "outlines";
	public static final boolean OUTLINES_DEFAULT = false;

	public static final String CELL_SIZE_ALIAS = "cellSize";
	public static final int CELL_SIZE_DEFAULT = 80;			// result will be 50 + 30 = 80
	public static final int MIN_CELL_SIZE = 30;
	public static final int MAX_CELL_SIZE = 200 - MIN_CELL_SIZE;
	public static final String AUTO_CHANGE_MESH_ALIAS = "autoChangeMesh";
	public static final boolean AUTO_CHANGE_MESH_DEFAULT = true;
	public static final String AUTO_CHANGE_MESH_INTERVAL_ALIAS = "autoChangeMeshIntv";
	public static final int AUTO_CHANGE_MESH_INTERVAL_DEFAULT = 30;
	public static final int AUTO_CHANGE_MESH_INTERVAL_MIN = 10;								// 10 seconds

	public static final String ENABLE_GYROSCOPE_ALIAS = "gyroscope";
	public static final boolean ENABLE_GYROSCOPE_DEFAULT = true;
	public static final String ENABLE_UV_MOVEMENT_ALIAS = "uvmovement";
	public static final boolean ENABLE_UV_MOVEMENT_DEFAULT = true;
	public static final String GYROSCOPE_THRESHOLD_ALIAS = "gyroscopeThrs";
	public static final int GYROSCOPE_THRESHOLD_DEFAULT = 10;
	public static final int GYROSCOPE_THRESHOLD_MIN = 1;


	// Constructor
	public MeshLW(DeviceOrientation orientation, MeshColors colors) {
		MeshLW.orientation = orientation;
		this.colors = colors;
	}


	@Override
	public void create () {

		Gdx.app.setLogLevel(Application.LOG_NONE);

		NPOTsupport = Gdx.graphics.supportsExtension("GL_OES_texture_npot")
					  || Gdx.graphics.supportsExtension("GL_ARB_texture_non_power_of_two");

		// Load resolution and setup mesh
		resolution.set((float)Gdx.graphics.getWidth(), (float)Gdx.graphics.getHeight());
		projection.setToOrtho2D(0f, 0f, resolution.x, resolution.y);
		mesh = setupMesh();

		// Async
		setupTriangulators();
		setupRunnables();

		// Load shader
		shader = new ShaderProgram(Gdx.files.internal("mesh.vert").readString(),
								   Gdx.files.internal("mesh.frag").readString());

		if (shader.getLog().length() != 0)
			Gdx.app.log(TAG, shader.getLog());

		// Load preferences
		preferences = Gdx.app.getPreferences(PREFS_NAME);
		setupPreferences();

		paused = false;

	}

	@Override
	public void render () {

		Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);

		if(!paused) {

			// Handle orientation
			if(gyroscopeEnabled == true)
				updateGyroscope();

			if(gyroscopeUVmove == true)
				updateUVmove();

			if(autoChangeColor == true)
				updateColor();

			if(autoChangeMesh == true)
				updateMesh();

			shader.begin();
			shader.setUniformMatrix("u_projTrans", projection);
			shader.setUniformf("u_resolution", resolution);
			shader.setUniformf("u_light", lightPosition);
			shader.setUniformf("u_uvmove", uvMove);
			shader.setUniformf("u_color0", color0);
			shader.setUniformf("u_color1", color1);
			shader.setUniformi("u_normal1", 1);
			shader.setUniformi("u_normal0", 0);
			shader.setUniformf("u_factor", factor);

			mesh.render(shader, GL20.GL_TRIANGLES);

			shader.end();

		}
	}
	
	@Override
	public void dispose () {

		if(shader != null) shader.dispose();
		if(mesh != null) mesh.dispose();
		if(triangulator0 != null) triangulator0.dispose();
		if(triangulator1 != null) triangulator1.dispose();

	}

	@Override
	public void resize(int w, int h) {
		// if this was only onVisibilityChanged call and resolution actually doesn't change
		if(resolution.x != (float)w) {
			resolution.set((float) w, (float) h);

			if (mesh != null) mesh.dispose();
			mesh = setupMesh();

			// renew touchscreen or gyroscope for new resolution
			if(rotation != null) rotation.setInitialPosition(getCenterOfScreen());
			if(touchscreen != null) touchscreen.setInitialPosition(getCenterOfScreen());

			projection.setToOrtho2D(0f, 0f, resolution.x, resolution.y);

			triangulator0.setResolution(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), NPOTsupport);
			triangulator1.setResolution(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), NPOTsupport);

			// create or regenerate normal map on orientation change
			setupNewNormalMap();
		}
	}

	@Override
	public void pause() {
		paused = true;
	}

	@Override
	public void resume() {

		if(preferencesChanged == true)
			setupPreferences();

		paused = false;
	}

	// Setters
	public static void setPreferencesChanged(boolean state) {
		preferencesChanged = state;
	}

	// Getters
	public int getCellSize() {
		return cellSize;
	}

	public boolean getOutlines() {
		return outlines;
	}

	private Vector3 decodeColor(int color) {
		return new Vector3((float)(color >>> 16 & 0xFF) / 255,
						   (float)(color >>> 8 & 0xFF) / 255,
						   (float)(color & 0xFF) / 255);
	}

	private Vector3 getCenterOfScreen() {
		return new Vector3((float)Gdx.graphics.getWidth() / 2,
			   			   (float)Gdx.graphics.getHeight() / 2,
						   lightZ);
	}

	float factor = 0f;
	float increment = 0.01f;
	private void updateMesh() {

		lastMeshChangeTime += Gdx.graphics.getDeltaTime();

		if(lastMeshChangeTime >= autoChangeMeshIntv) {

			factor += increment;

			if (factor >= 1f) {
				lastMeshChangeTime = 0f;
				factor = 1f;
				increment = -0.01f;
				new Thread(runnable0).start();
			}

			if (factor <= 0f) {
				lastMeshChangeTime = 0f;
				factor = 0f;
				increment = 0.01f;
				new Thread(runnable1).start();
			}
		}
	}

	// When checked option of automatic mesh change setup triangulators
	private void setupTriangulators() {

		triangulator0 = new Triangulator(this, projection);
		triangulator0.setResolution(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), NPOTsupport);

		triangulator1 = new Triangulator(this, projection);
		triangulator1.setResolution(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), NPOTsupport);

	}

	// if auto change mesh enabled set current visible normal to 0 and regenerate it
	// or regenerate normal0 alone
	private void setupNewNormalMap() {

		if(autoChangeMesh == true) {
			factor = 0f;
			lastMeshChangeTime = 0f;
			increment = 0.01f;
			new Thread(runnable1).start();
		}

		triangulator0.setResolution(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), NPOTsupport);
		triangulator0.bindNormalMap(0);

	}

	// Runnables to run in thread and generate normal0, norma11 respectively
	// Implement two runnables to avoid if statement which will be necessary for single runnable implementation
	private void setupRunnables() {

		runnable0 = new Runnable() {
			@Override
			public void run() {
				triangulator0.triangulate();
				Gdx.app.postRunnable(new Runnable() {
					@Override
					public void run() {
						triangulator0.render(0);
					}
				});
			}
		};

		runnable1 = new Runnable() {
			@Override
			public void run() {
				triangulator1.triangulate();
				Gdx.app.postRunnable(new Runnable() {
					@Override
					public void run() {
						triangulator1.render(1);
					}
				});
			}
		};
	}

	// If autocolor update enabled, changed colors
	private void updateColor() {

		lastColorChangeTime += Gdx.graphics.getDeltaTime();

		if (lastColorChangeTime >= autoChangeColorIntv) {
			if(colorMode == COLOR_MODE_RANDOM_COLOR) {
				int c0 = colors.getRandomColor();
				newColor0 = decodeColor(c0);
				preferences.putInteger(COLOR_0_ALIAS, c0);
				preferences.flush();
			} else if(colorMode == COLOR_MODE_RANDOM_GRADIENT) {
				int c0 = colors.getRandomColor();
				int c1 = colors.getRandomColor();
				newColor0 = decodeColor(c0);
				newColor1 = decodeColor(c1);
				preferences.putInteger(COLOR_0_ALIAS, c0);
				preferences.putInteger(COLOR_1_ALIAS, c1);
				preferences.flush();
			}

			lastColorChangeTime = 0f;
		}

		if(newColor0.len() != color0.len())
			color0.lerp(newColor0, Gdx.graphics.getDeltaTime());

		if(newColor1.len() != color1.len())
			color1.lerp(newColor1, Gdx.graphics.getDeltaTime());

	}

	private void updateUVmove() {

		uvMove.x = lightPosition.x;
		uvMove.y = lightPosition.y;

	}

	private void setGyroscopeUVmove(boolean enabled) {

		// nullable uv move coordinates
		if(enabled == false)
			uvMove.set(0f, 0f);

		gyroscopeUVmove = enabled;
	}

	// if gyroscope enabled update handler and set light position to lightPosition variable
	private void updateGyroscope() {

		if(orientation.isAvailable() == true) {
			rotation.update();
			lightPosition = rotation.getPosition();
		} else {
			touchscreen.update();
			lightPosition = touchscreen.getPosition();
		}
	}

	// If enabled enabled allow parallax
	// if orientation vector is not available set and unset touchscreenprocessor accordingly
	// if orientation vector available register and unregister orientation vector listener
	private void setGyroscope(boolean enabled) {

		if(enabled == false) {
			if (orientation.isAvailable() == false) {
				Gdx.input.setInputProcessor(null);
			} else {
				orientation.pause();
			}
		} else {
			if (orientation.isAvailable() == false) {
				if(touchscreen == null) touchscreen = new com.fasthamster.meshlwpro.Touchscreen();
				// this code run from setupPreferences so lightPosition now the center of the screen
				touchscreen.setInitialPosition(lightPosition);
				Gdx.input.setInputProcessor(touchscreen);
			} else {
				if(rotation == null) rotation = new com.fasthamster.meshlwpro.Rotation(orientation);
				rotation.setInitialPosition(lightPosition);
				orientation.resume();
			}
		}

		gyroscopeEnabled = enabled;

	}

	private void setGyroscopeThreshold(int t) {

		if(rotation != null)
			rotation.setThreshold((float)t);
	}

	private boolean normalNeedUpdate = false;
	private void setupPreferences() {
		// Colors preferences
		colorMode = preferences.getInteger(COLOR_MODE_ALIAS, COLOR_MODE_DEFAULT);

		if(colorMode == COLOR_MODE_COLOR || colorMode == COLOR_MODE_RANDOM_COLOR) {
			color0 = decodeColor(preferences.getInteger(COLOR_0_ALIAS, COLOR_0_DEFAULT));
			color1 = color0;
			newColor0 = newColor1 = color0;
		} else {
			color0 = decodeColor(preferences.getInteger(COLOR_0_ALIAS, COLOR_0_DEFAULT));
			color1 = decodeColor(preferences.getInteger(COLOR_1_ALIAS, COLOR_1_DEFAULT));
			newColor0 = color0;
			newColor1 = color1;
		}

		lightZ = (float)(preferences.getInteger(LIGHT_Z_ALIAS, LIGHT_Z_DEFAULT) + LIGHT_Z_MIN) / 10f;

		// if outlines state was changed set outlines state, update normal map
		if(outlines != preferences.getBoolean(OUTLINES_ALIAS, OUTLINES_DEFAULT)) {
			outlines = preferences.getBoolean(OUTLINES_ALIAS, OUTLINES_DEFAULT);
			normalNeedUpdate = true;
		}

		autoChangeColor = preferences.getBoolean(AUTO_CHANGE_COLOR_ALIAS, AUTO_CHANGE_COLOR_DEFAULT);
		autoChangeColorIntv = (float)preferences.getInteger(AUTO_CHANGE_COLOR_INTERVAL_ALIAS, AUTO_CHANGE_COLOR_INTERVAL_DEFAULT);
		// light position
		lightPosition.set(getCenterOfScreen());

		// Mesh preferences
		// Auto change mesh values used by setupNewNormalMap()
		autoChangeMesh = preferences.getBoolean(AUTO_CHANGE_MESH_ALIAS, AUTO_CHANGE_MESH_DEFAULT);
		autoChangeMeshIntv = preferences.getInteger(AUTO_CHANGE_MESH_INTERVAL_ALIAS, AUTO_CHANGE_MESH_INTERVAL_DEFAULT);

		// If cell size changed regenerate normal map
		if(cellSize - MIN_CELL_SIZE != preferences.getInteger(CELL_SIZE_ALIAS, CELL_SIZE_DEFAULT)) {
			// on min value pointer not on left side, it moved a bit right
			// so in preferences we hold values from 0 to 170
			// but when use it or show to user add MIN_CELL_SIZE to get correct value from 30-200 range
			cellSize = preferences.getInteger(CELL_SIZE_ALIAS, CELL_SIZE_DEFAULT) + MIN_CELL_SIZE;
			normalNeedUpdate = true;
		}

		// because normal may be updated in two cases, when cell size was changed and when outlines was enabled/disabled
		// to avoid double call of setupNewNormalMap, i use normalNeedUpdate variable
		if(normalNeedUpdate == true) {
			setupNewNormalMap();
			normalNeedUpdate = false;
		}

		// Gyroscope preferences
		setGyroscope(preferences.getBoolean(ENABLE_GYROSCOPE_ALIAS, ENABLE_GYROSCOPE_DEFAULT));
		setGyroscopeUVmove(preferences.getBoolean(ENABLE_UV_MOVEMENT_ALIAS, ENABLE_UV_MOVEMENT_DEFAULT));
		setGyroscopeThreshold(preferences.getInteger(GYROSCOPE_THRESHOLD_ALIAS, GYROSCOPE_THRESHOLD_DEFAULT));


		preferencesChanged = false;

	}

	// Generate mesh
	private Mesh setupMesh() {

		Mesh mesh = new Mesh(true,
				4, 6,
				new VertexAttribute(VertexAttributes.Usage.Position, 3, "a_position"),
				new VertexAttribute(VertexAttributes.Usage.TextureCoordinates, 2, "a_texCoord0"));

		mesh.setVertices(new float[]{           // Vertices + UVs
				resolution.x, 0f, 0f, 0f, 0.8f,
				0f, 0f, 0f, 0.8f, 0.8f,
				0f, resolution.y, 0f, 0.8f, 0f,
				resolution.x, resolution.y, 0f, 0f, 0f});

		mesh.setIndices(new short[]{0, 1, 2, 2, 3, 0});

		return mesh;

	}
}
