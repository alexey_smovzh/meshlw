package com.fasthamster.meshlwpro;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.DelaunayTriangulator;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.ShortArray;

/**
 * Created by alex on 05.12.16.
 */

public class Triangulator {

    private MeshLW parent;
    private ShapeRenderer renderer;
    private FrameBuffer frameBuffer;
    private Matrix4 projection;

    private DelaunayTriangulator triangulator;

    private FloatArray points = new FloatArray();
    private ShortArray triangles;
    private int width, height;

    private static final float BIAS = 0.2f;
    private static final float BIAS2 = 0.4f;
    private static final float LINE_WIDTH = 2f;


    private static final Color[] palette = {
            new Color(0.373f, 0.431f, 0.984f, 1.000f),
            new Color(0.475f, 0.404f, 0.988f, 1.000f),
            new Color(0.412f, 0.427f, 0.996f, 1.000f),
            new Color(0.514f, 0.400f, 1.000f, 1.000f),
            new Color(0.455f, 0.420f, 1.000f, 1.000f),
            new Color(0.333f, 0.467f, 0.980f, 1.000f),
            new Color(0.573f, 0.396f, 0.988f, 1.000f),
            new Color(0.388f, 0.455f, 0.984f, 1.000f),
            new Color(0.553f, 0.412f, 0.992f, 1.000f),
            new Color(0.502f, 0.435f, 0.996f, 1.000f),
            new Color(0.475f, 0.447f, 0.996f, 1.000f),
            new Color(0.447f, 0.459f, 1.000f, 1.000f),
            new Color(0.424f, 0.475f, 1.000f, 1.000f),
            new Color(0.604f, 0.439f, 0.992f, 1.000f),
            new Color(0.427f, 0.494f, 0.973f, 1.000f),
            new Color(0.373f, 0.510f, 0.996f, 1.000f),
            new Color(0.400f, 0.502f, 1.000f, 1.000f),
            new Color(0.435f, 0.494f, 1.000f, 1.000f),
            new Color(0.557f, 0.459f, 1.000f, 1.000f),
            new Color(0.502f, 0.478f, 1.000f, 1.000f),
            new Color(0.529f, 0.471f, 1.000f, 1.000f),
            new Color(0.475f, 0.490f, 1.000f, 1.000f),
            new Color(0.596f, 0.471f, 0.996f, 1.000f),
            new Color(0.573f, 0.482f, 1.000f, 1.000f),
            new Color(0.545f, 0.494f, 1.000f, 1.000f),
            new Color(0.624f, 0.475f, 0.984f, 1.000f),
            new Color(0.490f, 0.514f, 1.000f, 1.000f),
            new Color(0.365f, 0.553f, 0.984f, 1.000f),
            new Color(0.522f, 0.506f, 1.000f, 1.000f),
            new Color(0.671f, 0.467f, 0.965f, 1.000f),
            new Color(0.463f, 0.525f, 1.000f, 1.000f),
            new Color(0.498f, 0.529f, 0.992f, 1.000f),
            new Color(0.584f, 0.506f, 0.996f, 1.000f),
            new Color(0.624f, 0.498f, 0.984f, 1.000f),
            new Color(0.561f, 0.518f, 0.996f, 1.000f),
            new Color(0.451f, 0.557f, 1.000f, 1.000f),
            new Color(0.478f, 0.549f, 1.000f, 1.000f),
            new Color(0.420f, 0.580f, 0.988f, 1.000f),
            new Color(0.600f, 0.529f, 0.992f, 1.000f),
            new Color(0.647f, 0.525f, 0.973f, 1.000f),
            new Color(0.522f, 0.561f, 0.996f, 1.000f),
            new Color(0.576f, 0.545f, 0.996f, 1.000f),
            new Color(0.553f, 0.557f, 1.000f, 1.000f),
            new Color(0.584f, 0.561f, 0.984f, 1.000f),
            new Color(0.639f, 0.545f, 0.988f, 1.000f),
            new Color(0.682f, 0.537f, 0.969f, 1.000f),
            new Color(0.616f, 0.557f, 0.992f, 1.000f),
            new Color(0.455f, 0.608f, 0.984f, 1.000f),
            new Color(0.533f, 0.584f, 0.992f, 1.000f),
            new Color(0.486f, 0.604f, 0.992f, 1.000f),
            new Color(0.608f, 0.576f, 0.976f, 1.000f),
            new Color(0.424f, 0.643f, 0.992f, 1.000f),
            new Color(0.565f, 0.604f, 0.988f, 1.000f),
            new Color(0.600f, 0.612f, 0.976f, 1.000f),
            new Color(0.686f, 0.596f, 0.961f, 1.000f),
            new Color(0.549f, 0.655f, 0.976f, 1.000f),
            new Color(0.624f, 0.635f, 0.976f, 1.000f),
            new Color(0.627f, 0.651f, 0.965f, 1.000f),
    };

    // Constructor
    public Triangulator(MeshLW m, Matrix4 p) {

        this.parent = m;
        this.projection = p;

        renderer = new ShapeRenderer();
        triangulator = new DelaunayTriangulator();

    }

    public void bindNormalMap(int slot) {
        triangulate();
        render(slot);
    }

    // if  npot support false, set resolution to power of two size
    // 512, 1024, 2048, 4096
    public void setResolution(int w, int h, boolean npot) {

        if(npot == false) {
            this.width = convertPOT(w);
            this.height = convertPOT(h);
        } else {
            this.width = w;
            this.height = h;
        }

        if(frameBuffer != null)
            frameBuffer.dispose();
        // MSAA is not working on custom framebuffer
        // so create texture in 4 times bigger than needed, after downsampling it will look like antialiased
//        frameBuffer = new FrameBuffer(Pixmap.Format.RGB888, w * 2, h * 2, false);
        frameBuffer = new FrameBuffer(Pixmap.Format.RGBA8888, w * 2, h * 2, false);

    }

    // http://stackoverflow.com/questions/4398711/round-to-the-nearest-power-of-two
    private int convertPOT(int number) {

        number--;
        number |= number >> 1;
        number |= number >> 2;
        number |= number >> 4;
        number |= number >> 8;
        number |= number >> 16;
        number++;

        return number;
    }

    private int cellSize;
    // Generate point array with random points positions
    public void triangulate() {

        if(points != null) points.clear();
        if(triangles != null) triangles.clear();

        cellSize = parent.getCellSize();

        for(int horizontal = -cellSize; horizontal < width + cellSize; horizontal += cellSize) {
            for (int vertical = -cellSize; vertical < height + cellSize; vertical += cellSize * 2f) {   // vertically cells longer
                points.add(MathUtils.random(horizontal, horizontal + cellSize));
                points.add(MathUtils.random(vertical, vertical + cellSize));
            }
        }

        triangles = triangulator.computeTriangles(points, false);
    }

    public void render(int slot) {

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

        frameBuffer.begin();

        renderer.setProjectionMatrix(projection);
        renderer.begin(ShapeRenderer.ShapeType.Filled);

        for (int i = 0; i < triangles.size; i += 3) {
            int p1 = triangles.get(i) * 2;
            int p2 = triangles.get(i + 1) * 2;
            int p3 = triangles.get(i + 2) * 2;

            renderer.setColor(palette[MathUtils.random(palette.length-1)]);
            renderer.triangle(points.get(p1), points.get(p1 + 1),
                              points.get(p2), points.get(p2 + 1),
                              points.get(p3), points.get(p3 + 1));
        }

        renderer.end();

        // outlines
        // fake antialiasing by drawing lines four times overlapping with alpha transparency
        if(parent.getOutlines() == true) {
            Gdx.gl.glLineWidth(LINE_WIDTH);
            renderer.setColor(0.5f, 0.5f, 1.0f, 0.04f);
            renderer.begin(ShapeRenderer.ShapeType.Line);

            for (int i = 0; i < triangles.size; i += 3) {
                int p1 = triangles.get(i) * 2;
                int p2 = triangles.get(i + 1) * 2;
                int p3 = triangles.get(i + 2) * 2;

                renderer.triangle(
                        points.get(p1) + BIAS, points.get(p1 + 1) + BIAS,
                        points.get(p2) + BIAS, points.get(p2 + 1) + BIAS,
                        points.get(p3) + BIAS, points.get(p3 + 1) + BIAS);

                renderer.triangle(
                        points.get(p1) - BIAS, points.get(p1 + 1) - BIAS,
                        points.get(p2) - BIAS, points.get(p2 + 1) - BIAS,
                        points.get(p3) - BIAS, points.get(p3 + 1) - BIAS);

                renderer.triangle(
                        points.get(p1) + BIAS2, points.get(p1 + 1) + BIAS2,
                        points.get(p2) + BIAS2, points.get(p2 + 1) + BIAS2,
                        points.get(p3) + BIAS2, points.get(p3 + 1) + BIAS2);

                renderer.triangle(
                        points.get(p1) - BIAS2, points.get(p1 + 1) - BIAS2,
                        points.get(p2) - BIAS2, points.get(p2 + 1) - BIAS2,
                        points.get(p3) - BIAS2, points.get(p3 + 1) - BIAS2);
            }
            renderer.end();
        }

        frameBuffer.end();

        frameBuffer.getColorBufferTexture().bind(slot);
    }

    public void dispose() {

        if(renderer != null) renderer.dispose();
        if(frameBuffer != null) frameBuffer.dispose();

        if(points != null) points.clear();
        if(triangles != null) triangles.clear();

    }
}
