package com.fasthamster.meshlwpro;

/**
 * Created by alex on 09.12.16.
 */

public interface MeshColors {
    int getRandomColor();
    int getRandomColor(int color);
}
