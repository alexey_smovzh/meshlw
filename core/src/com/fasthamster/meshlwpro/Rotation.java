package com.fasthamster.meshlwpro;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by alex on 07.12.16.
 */

public class Rotation {

    private final DeviceOrientation orientation;
    private float[] initialDeviceOrientation = new float[2];

    private Vector3 position = new Vector3();
    private Vector3 centerScreen = new Vector3();
    private float threshold;
    private float width, height;

    private static final float ALPHA = 0.02f;


    public Rotation(DeviceOrientation orientation) {

        this.orientation = orientation;

        System.arraycopy(orientation.getOrientation(), 0, this.initialDeviceOrientation, 0, 2);

    }

    // Getter
    public Vector3 getPosition() {
        return position;
    }

    // Setter
    public void setInitialPosition(Vector3 p) {

        this.position.set(p);
        this.centerScreen.set(p);

        this.width = (float)Gdx.graphics.getWidth();
        this.height = (float)Gdx.graphics.getHeight();

    }

    public void setThreshold(float t) {
        this.threshold = t;
    }

    private void process(float deltaX, float deltaY) {

        position.x += deltaX * threshold;
        if(position.x < 0f) position.x = 0f;                // check if resulting position not out of the screen
        if(position.x > width) position.x = width;

        position.y += -(deltaY * threshold);                // invert Y coord
        if(position.y < 0) position.y = 0f;
        if(position.y > height) position.y = height;

    }

    float diffPitch, diffRoll;
    public void update() {

        float[] o = orientation.getOrientation();

        diffPitch = initialDeviceOrientation[0] - o[0];
        diffRoll = initialDeviceOrientation[1] - o[1];

        // Save current device orientation
        initialDeviceOrientation[0] = o[0];
        initialDeviceOrientation[1] = o[1];

        if (Math.abs(diffPitch) + Math.abs(diffRoll) > 0.1f)
            process(diffPitch, diffRoll);

        // Return camera to initial position
        // fuzzy equality testing
        if(position.epsilonEquals(centerScreen, 1f) == false)
            position.interpolate(centerScreen, ALPHA, Interpolation.linear);

    }
}