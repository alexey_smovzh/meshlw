#ifdef GL_ES
    #define LOWP lowp
    #define MED mediump
    #define HIGH highp
    precision mediump float;
#else
    #define MED
    #define LOWP
    #define HIGH
#endif

attribute vec4 a_position;
attribute vec2 a_texCoord0;

uniform mat4 u_projTrans;
uniform vec2 u_uvmove;                      // light.xy position

varying vec2 v_uv;

void main() {

    v_uv = a_texCoord0;
    v_uv += (u_uvmove / 12000.0);         // move texture followed device rotation

    gl_Position = u_projTrans * a_position;

}
