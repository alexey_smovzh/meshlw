#ifdef GL_ES
    #define LOWP lowp
    #define MED mediump
    #define HIGH highp
    precision lowp float;
#else
    #define MED
    #define LOWP
    #define HIGH
#endif


uniform sampler2D u_normal0;
uniform sampler2D u_normal1;
uniform float u_factor;
uniform vec2 u_resolution;
uniform vec3 u_light;               // light position
uniform vec3 u_color0;
uniform vec3 u_color1;


varying vec2 v_uv;

#define ROUGHNESS 0.04      // 0.2 * 0.2


void main(void) {

    vec2 position = gl_FragCoord.xy / u_resolution.xy;

    vec4 normal0 = texture2D(u_normal0, v_uv);
    vec4 normal1 = texture2D(u_normal1, v_uv);
    vec4 normal = mix(normal0, normal1, u_factor);
//    normal.g = 1.0 - normal.g;

    vec3 lightPos = vec3(u_light.x / u_resolution.x,
                         u_light.y / u_resolution.y,
                         u_light.z);

    vec3 lightDir = vec3(lightPos.xy - position, lightPos.z);
    lightDir.x *= u_resolution.x / u_resolution.y;

/*
    // directional light
    vec2 center = u_resolution.xy / 2.0;
    vec2 direction = normalize(u_light.xy - center);
    vec3 lightDir = vec3(direction.xy, u_light.z);
*/

    vec3 N = normalize(normal.rgb * 2.0 - 1.0);
    vec3 L = normalize(lightDir);

    position.y *= u_resolution.y / u_resolution.x;          // Correct aspect ratio
    vec3 gradient = vec3(mix(u_color1, u_color0, position.y));

    // oren-nayar http://ruh.li/GraphicsOrenNayar.html
    vec3 E = normalize(vec3(u_resolution.xy/2.0, 1.0));     // eye dir

    float NdotL = dot(N, L);
    float NdotV = dot(N, E);

    float angleVN = acos(NdotV);
    float angleLN = acos(NdotL);

    float alpha = max(angleVN, angleLN);
    float beta = min(angleVN, angleLN);
    float gamma = dot(E - N * dot(E, N), L - N * dot(L, N));

    float A = 1.0 - 0.5 * (ROUGHNESS / (ROUGHNESS + 0.57));
    float B = 0.45 * (ROUGHNESS / (ROUGHNESS + 0.09));
    float C = sin(alpha) * tan(beta);

    float L1 = max(0.0, NdotL) * (A + B * max(0.0, gamma) * C);

    gl_FragColor = mix(vec4(0.0), vec4(gradient * L1, 1.0), normal.a);

}
