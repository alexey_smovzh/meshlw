package com.fasthamster.meshlwpro;

import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.badlogic.gdx.backends.android.AndroidLiveWallpaperService;
import com.badlogic.gdx.backends.android.AndroidWallpaperListener;

/**
 * Created by alex on 07.12.16.
 */

public class LiveWallpaper extends AndroidLiveWallpaperService {

    public static SettingsHandler settings;
    public static ColorHandler color;
    private OrientationHandler handler;
    private MeshLWListener listener;


    @Override
    public void onCreateApplication() {

        super.onCreateApplication();

        // Initialize gyroscope
        handler = new OrientationHandler(getApplicationContext());

        // Colors
        color = new ColorHandler();

        // Settings staff
        settings = new SettingsHandler(getApplicationContext());
        settings.setGyroscopeAvailable(handler.isAvailable());

        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        config.useCompass = false;
        config.useWakelock = false;
        config.getTouchEventsForLiveWallpaper = false;
        config.useAccelerometer = handler.isAvailable();

        listener = new MeshLWListener(handler, color);

        initialize(listener, config);

    }

    @Override
    public Engine onCreateEngine() {
        return new AndroidWallpaperEngine(){
            @Override
            public void onPause(){
                super.onPause();
                listener.pause();
                handler.pause();
            }

            @Override
            public void onResume(){
                super.onResume();
                handler.resume();
            }
        };
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    public static class MeshLWListener extends MeshLW implements AndroidWallpaperListener {

        public MeshLWListener(DeviceOrientation orientation, MeshColors colors) {
            super(orientation, colors);
        }


        @Override
        public void offsetChange(float xOffset, float yOffset, float xOffsetStep, float yOffsetStep, int xPixelOffset, int yPixelOffset) {

        }

        @Override
        public void previewStateChange(boolean isPreview) {

        }
    }
}
