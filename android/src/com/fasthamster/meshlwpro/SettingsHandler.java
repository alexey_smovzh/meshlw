package com.fasthamster.meshlwpro;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by alex on 07.12.16.
 */

public class SettingsHandler {

    Context context;
    SharedPreferences settings;
    SharedPreferences.Editor editor;

    private boolean isGyroscopeAvailable;
    private boolean preferencesChanged;


    private int colorMode;
    private int contrast;
    private int color0;
    private int color1;
    private boolean autoColor;
    private int autoColorInterval;
    private boolean outlines;
    private int cell;
    private boolean autoMesh;
    private int autoMeshInterval;
    private boolean gyroscopeEnable;
    private boolean gyroscopeUVmove;
    private int gyroscopeThreshold;



    public SettingsHandler(Context context) {
        this.context = context;
        load();
    }

    public void load() {

        settings = context.getSharedPreferences(MeshLW.PREFS_NAME, 0);
        colorMode = settings.getInt(MeshLW.COLOR_MODE_ALIAS, MeshLW.COLOR_MODE_DEFAULT);
        contrast = settings.getInt(MeshLW.LIGHT_Z_ALIAS, MeshLW.LIGHT_Z_DEFAULT);
        color0 = settings.getInt(MeshLW.COLOR_0_ALIAS, MeshLW.COLOR_0_DEFAULT);
        color1 = settings.getInt(MeshLW.COLOR_1_ALIAS, MeshLW.COLOR_1_DEFAULT);
        autoColor = settings.getBoolean(MeshLW.AUTO_CHANGE_COLOR_ALIAS, MeshLW.AUTO_CHANGE_COLOR_DEFAULT);
        autoColorInterval = settings.getInt(MeshLW.AUTO_CHANGE_COLOR_INTERVAL_ALIAS, MeshLW.AUTO_CHANGE_COLOR_INTERVAL_DEFAULT);
        outlines = settings.getBoolean(MeshLW.OUTLINES_ALIAS, MeshLW.OUTLINES_DEFAULT);
        cell = settings.getInt(MeshLW.CELL_SIZE_ALIAS, MeshLW.CELL_SIZE_DEFAULT);
        autoMesh = settings.getBoolean(MeshLW.AUTO_CHANGE_MESH_ALIAS, MeshLW.AUTO_CHANGE_MESH_DEFAULT);
        autoMeshInterval = settings.getInt(MeshLW.AUTO_CHANGE_MESH_INTERVAL_ALIAS, MeshLW.AUTO_CHANGE_MESH_INTERVAL_DEFAULT);
        gyroscopeEnable = settings.getBoolean(MeshLW.ENABLE_GYROSCOPE_ALIAS, MeshLW.ENABLE_GYROSCOPE_DEFAULT);
        gyroscopeUVmove = settings.getBoolean(MeshLW.ENABLE_UV_MOVEMENT_ALIAS, MeshLW.ENABLE_UV_MOVEMENT_DEFAULT);
        gyroscopeThreshold = settings.getInt(MeshLW.GYROSCOPE_THRESHOLD_ALIAS, MeshLW.GYROSCOPE_THRESHOLD_DEFAULT);

        preferencesChanged = false;
    }

    public void save() {

        editor = settings.edit();
        editor.putInt(MeshLW.COLOR_MODE_ALIAS, colorMode);
        editor.putInt(MeshLW.LIGHT_Z_ALIAS, contrast);
        editor.putInt(MeshLW.COLOR_0_ALIAS, color0);
        editor.putInt(MeshLW.COLOR_1_ALIAS, color1);
        editor.putBoolean(MeshLW.AUTO_CHANGE_COLOR_ALIAS, autoColor);
        editor.putInt(MeshLW.AUTO_CHANGE_COLOR_INTERVAL_ALIAS, autoColorInterval);
        editor.putBoolean(MeshLW.OUTLINES_ALIAS, outlines);
        editor.putInt(MeshLW.CELL_SIZE_ALIAS, cell);
        editor.putBoolean(MeshLW.AUTO_CHANGE_MESH_ALIAS, autoMesh);
        editor.putInt(MeshLW.AUTO_CHANGE_MESH_INTERVAL_ALIAS, autoMeshInterval);
        editor.putBoolean(MeshLW.ENABLE_GYROSCOPE_ALIAS, gyroscopeEnable);
        editor.putBoolean(MeshLW.ENABLE_UV_MOVEMENT_ALIAS, gyroscopeUVmove);
        editor.putInt(MeshLW.GYROSCOPE_THRESHOLD_ALIAS, gyroscopeThreshold);
        editor.commit();

        preferencesChanged = false;

    }

    public void loadDefaults() {

        setColorMode(MeshLW.COLOR_MODE_DEFAULT);
        setContrast(MeshLW.LIGHT_Z_DEFAULT);
        setColor0(MeshLW.COLOR_0_DEFAULT);
        setColor1(MeshLW.COLOR_1_DEFAULT);
        setAutoColor(MeshLW.AUTO_CHANGE_COLOR_DEFAULT);
        setAutoColorInterval(MeshLW.AUTO_CHANGE_COLOR_INTERVAL_DEFAULT);
        setOutlines(MeshLW.OUTLINES_DEFAULT);
        setCell(MeshLW.CELL_SIZE_DEFAULT - MeshLW.MIN_CELL_SIZE);                   // offset fix for seekbar
        setAutoMesh(MeshLW.AUTO_CHANGE_MESH_DEFAULT);
        setAutoMeshInterval(MeshLW.AUTO_CHANGE_MESH_INTERVAL_DEFAULT);
        setGyroscopeEnable(MeshLW.ENABLE_GYROSCOPE_DEFAULT);
        setGyroscopeUVmove(MeshLW.ENABLE_UV_MOVEMENT_DEFAULT);
        setGyroscopeThreshold(MeshLW.GYROSCOPE_THRESHOLD_DEFAULT);

    }

    public Boolean isPreferencesChanged() {

        return preferencesChanged;

    }

    // runtime variable
    public boolean getGyroscopeAvailable() {
        return this.isGyroscopeAvailable;
    }
    public void setGyroscopeAvailable(boolean g) {
        this.isGyroscopeAvailable = g;
    }

    public int getColorMode() {
        return colorMode;
    }
    public int getContrast() {
        return contrast;
    }
    public int getColor0() {
        return color0;
    }
    public int getColor1() {
        return color1;
    }
    public boolean getAutoColor() {
        return autoColor;
    }
    public int getAutoColorInterval() {
        return autoColorInterval;
    }
    public boolean getOutlines(){
        return outlines;
    }
    public int getCell() {
        return  cell;
    }
    public boolean getAutoMesh() {
        return autoMesh;
    }
    public int getAutoMeshInterval() {
        return autoMeshInterval;
    }
    public boolean getGyroscopeEnable() {
        return gyroscopeEnable;
    }
    public boolean getGyroscopeUVmove() {
        return gyroscopeUVmove;
    }
    public int getGyroscopeThreshold() {
        return gyroscopeThreshold;
    }


    public void setColorMode(int m) {
        this.colorMode = m;
        this.preferencesChanged = true;
    }
    public void setContrast(int c) {
        this.contrast = c;
        this.preferencesChanged = true;
    }
    public void setColor0(int c0) {
        this.color0 = c0;
        this.preferencesChanged = true;
    }
    public void setColor1(int c1) {
        this.color1 = c1;
        this.preferencesChanged = true;
    }
    public void setAutoColor(boolean a) {
        this.autoColor = a;
        this.preferencesChanged = true;
    }
    public void setAutoColorInterval(int i) {
        this.autoColorInterval = i;
        this.preferencesChanged = true;
    }
    public void setOutlines(boolean o) {
        this.outlines = o;
        this.preferencesChanged = true;
    }
    public void setCell(int l) {
        this.cell = l;
        this.preferencesChanged = true;
    }
    public void setAutoMesh(boolean m) {
        this.autoMesh = m;
        this.preferencesChanged = true;
    }
    public void setAutoMeshInterval(int j) {
        this.autoMeshInterval = j;
        this.preferencesChanged = true;
    }
    public void setGyroscopeEnable(boolean g) {
        this.gyroscopeEnable = g;
        this.preferencesChanged = true;
    }
    public void setGyroscopeUVmove(boolean u) {
        this.gyroscopeUVmove = u;
        this.preferencesChanged = true;
    }
    public void setGyroscopeThreshold(int t) {
        this.gyroscopeThreshold = t;
        this.preferencesChanged = true;
    }
}
