package com.fasthamster.meshlwpro;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

/*
 * Created by alex on 26.12.16.
 */

public class GyroscopeSettingsFragment extends Fragment {

    private CheckBox enableGrsp;
    private CheckBox enableUVmove;
    private TextView enableUVtitle;
    private SeekBar grspThreshold;
    private TextView grspThresholdTitle, grspThresholdResult, grspThresholdResultMin, grspThresholdResultMax;
    private LinearLayout notHaveGrspMsg;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.gyroscope_settings, container, false);

        enableGrsp = (CheckBox)view.findViewById(R.id.chk_gyroscope_enable);
        enableUVmove = (CheckBox)view.findViewById(R.id.chk_uv_movement);
        enableUVtitle = (TextView)view.findViewById(R.id.tv_uv_movement_title);
        grspThreshold = (SeekBar)view.findViewById(R.id.sb_gyroscope_threshold);
        grspThresholdTitle = (TextView)view.findViewById(R.id.tv_gyroscope_threshold_title);
        grspThresholdResult = (TextView)view.findViewById(R.id.tv_gyroscope_threshold_result);
        grspThresholdResultMin = (TextView)view.findViewById(R.id.tv_gyroscope_threshold_min);
        grspThresholdResultMax = (TextView)view.findViewById(R.id.tv_gyroscope_threshold_max);
        notHaveGrspMsg = (LinearLayout)view.findViewById(R.id.ll_not_have_gyroscope);

        // gyroscope enable checkbox listener
        enableGrsp.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                LiveWallpaper.settings.setGyroscopeEnable(b);
                gyroscopeSettingsVisible(b);
            }
        });

        // enable UV movement listener
        enableUVmove.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                LiveWallpaper.settings.setGyroscopeUVmove(b);
            }
        });

        // gyroscope threshold listener
        grspThreshold.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {

                if(progress < MeshLW.GYROSCOPE_THRESHOLD_MIN)
                    progress = MeshLW.GYROSCOPE_THRESHOLD_MIN;

                if(b == true) {
                    LiveWallpaper.settings.setGyroscopeThreshold(progress);
                    grspThresholdResult.setText(String.valueOf(progress));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });


        return view;

    }

    @Override
    public void onStart() {

        super.onStart();
        // load widgets states
        enableGrsp.setChecked(LiveWallpaper.settings.getGyroscopeEnable());
        enableUVmove.setChecked(LiveWallpaper.settings.getGyroscopeUVmove());
        grspThreshold.setProgress(LiveWallpaper.settings.getGyroscopeThreshold());
        grspThresholdResult.setText(String.valueOf(LiveWallpaper.settings.getGyroscopeThreshold()));
        gyroscopeSettingsVisible(LiveWallpaper.settings.getGyroscopeEnable());

        if(LiveWallpaper.settings.getGyroscopeAvailable() == false) {
            notHaveGrspMsg.setVisibility(View.VISIBLE);
        } else {
            notHaveGrspMsg.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);

    }

    private void gyroscopeSettingsVisible(boolean visible) {

        enableUVmove.setEnabled(visible);
        enableUVtitle.setEnabled(visible);
        gyroscopeThresholdSettingsVisible(visible);
    }

    private void gyroscopeThresholdSettingsVisible(boolean visible) {

        // if gyroscope is unavailable it is alway unvisible
        if(LiveWallpaper.settings.getGyroscopeAvailable() == false)
            visible = false;

        grspThreshold.setEnabled(visible);
        grspThresholdTitle.setEnabled(visible);
        grspThresholdResult.setEnabled(visible);
        grspThresholdResultMin.setEnabled(visible);
        grspThresholdResultMax.setEnabled(visible);

    }
}
