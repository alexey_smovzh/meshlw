package com.fasthamster.meshlwpro;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import java.util.List;

/**
 * Created by alex on 07.12.16.
 */

public class OrientationHandler implements SensorEventListener, DeviceOrientation {

    private boolean exist;

    private SensorManager manager;
    private List<Sensor> sensors;
    Context context;

    private static float[] mRotationMatrix = new float[16];
    private static float[] values = new float[2];


    public OrientationHandler(Context context) {

        this.context = context;

        manager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        exist = registerOrientationListener();

    }

    private boolean registerOrientationListener() {

        sensors = manager.getSensorList(Sensor.TYPE_ROTATION_VECTOR);

        if(sensors.size() != 0) {
            manager.registerListener(this, sensors.get(0), SensorManager.SENSOR_DELAY_UI);
            return true;
        } else {
            return false;
        }
    }

    private float[] lowPass(float[] input, float[] output) {

        if(output == null) return input;

        output[0] = output[0] + 0.1f * (input[0] - output[0]);
        output[1] = output[1] + 0.1f * (input[1] - output[1]);
        output[2] = 0f;             // this value not used


        return output;
    }

    public boolean isAvailable() {
        return this.exist;
    }

    @Override
    public float[] getOrientation() {

        return values;

    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    private float[] sensorVals = new float[3];
    private float[] matrixValues = new float[3];
    @Override
    public void onSensorChanged(SensorEvent event) {

        sensorVals = lowPass(event.values, sensorVals);

        if(sensorVals != null) {

            SensorManager.getRotationMatrixFromVector(mRotationMatrix, sensorVals);

            SensorManager.getOrientation(mRotationMatrix, matrixValues);
            values[0] = (float) Math.toDegrees(matrixValues[1]);    // pitch
            values[1] = (float) Math.toDegrees(matrixValues[2]);    // roll

        }
    }

    @Override
    public void pause() {
        manager.unregisterListener(this);
    }

    @Override
    public void resume() {
        exist = registerOrientationListener();
    }
}


