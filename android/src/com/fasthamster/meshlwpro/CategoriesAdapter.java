package com.fasthamster.meshlwpro;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

/*
 * Created by alex on 26.12.16.
 */

public class CategoriesAdapter extends ArrayAdapter<CategoriesAdapter.Category> {

    // Holder
    public static class Category {
        public int name;
        public int description;

        public Category(int name, int description) {
            this.name = name;
            this.description = description;
        }
    }

    // Categories
    public static final ArrayList<Category> CATEGORIES = new ArrayList<Category>(Arrays.asList(
                                           new Category(R.string.color_category_title, R.string.color_category_description),
                                           new Category(R.string.mesh_category_title, R.string.mesh_category_description),
                                           new Category(R.string.interaction_category_title, R.string.interaction_category_description),
                                           new Category(R.string.defaults_category_title, R.string.defaults_category_description),
                                           new Category(R.string.marketing_category_title, R.string.marketing_category_description)));

    public final static int COLOR_CATEGORY = 0;
    public final static int MESH_CATEGORY = 1;
    public final static int GYROSCOPE_CATEGORY = 2;
    public final static int DEFAULTS_BUTTON_CATEGORY = 3;
    public final static int MARKETING_BUTTON_CATEGORY = 4;


    // Constructor
    public CategoriesAdapter(Context context, int resource, ArrayList<Category> categories) {
        super(context, resource, categories);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.category_row, parent, false);
        }

        Category cat = getItem(position);

        TextView tvName = (TextView) convertView.findViewById(R.id.tv_category_name);
        TextView tvDescription = (TextView) convertView.findViewById(R.id.tv_category_description);

        tvName.setText(cat.name);
        tvDescription.setText(cat.description);

        return convertView;
    }
}
