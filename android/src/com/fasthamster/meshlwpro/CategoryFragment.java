package com.fasthamster.meshlwpro;

import android.app.Activity;
import android.app.ListFragment;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

/*
 * Created by alex on 26.12.16.
 */


public class CategoryFragment extends ListFragment {

    OnCategorySelectedListener listener;

    public interface OnCategorySelectedListener {
        void onCategorySelected(int position);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        CategoriesAdapter adapter = new CategoriesAdapter(getActivity(), R.layout.category_row, CategoriesAdapter.CATEGORIES);
        setListAdapter(adapter);

    }

    @Override
    public void onStart() {

        super.onStart();

        if (getFragmentManager().findFragmentById(R.id.settings_fragment) != null)
            getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);

    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {

        super.onAttach(activity);

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) return;

        if (activity instanceof OnCategorySelectedListener) {
            listener = (OnCategorySelectedListener) activity;
        } else {
            throw new RuntimeException(activity.toString()
                    + " must implement OnCategorySelectedListener");
        }
    }

    @Override
    public void onAttach(Context context) {

        super.onAttach(context);

        if (context instanceof OnCategorySelectedListener) {
            listener = (OnCategorySelectedListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnCategorySelectedListener");
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {

        listener.onCategorySelected(position);
        getListView().setItemChecked(position, true);

    }
}
