package com.fasthamster.meshlwpro;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

/**
 * Created by alex on 07.12.16.
 */


public class SettingsActivity extends Activity implements CategoryFragment.OnCategorySelectedListener {

    private ColorSettingsFragment colorFragment;
    private MeshSettingsFragment meshFragment;
    private GyroscopeSettingsFragment gyroscopeFragment;

    private Fragment currentFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_layout);

        if(findViewById(R.id.fragment_container) != null) {

            if (savedInstanceState != null)
                return;

            CategoryFragment category = new CategoryFragment();
            getFragmentManager().beginTransaction().add(R.id.fragment_container, category).commit();

        }
    }

    // When SettingsActivity going to background save settings if they was changed
    @Override
    public void onPause() {
        savePreferences();
        super.onPause();
    }

    private void savePreferences() {
        // Check if preferences was actually changed
        if(LiveWallpaper.settings.isPreferencesChanged() != null
                && LiveWallpaper.settings.isPreferencesChanged() == true) {
            LiveWallpaper.settings.save();
            MeshLW.setPreferencesChanged(true);
        }
    }

    boolean dualPlane;
    public void onCategorySelected(int position) {

        View settingsFrame = findViewById(R.id.settings_fragment);
        dualPlane = settingsFrame != null && settingsFrame.getVisibility() == View.VISIBLE;

        int containerId = dualPlane == true ? R.id.settings_fragment : R.id.fragment_container;

        switch (position) {
            case CategoriesAdapter.COLOR_CATEGORY:
                setupColorFragment(containerId);
                break;
            case CategoriesAdapter.MESH_CATEGORY:
                setupMeshFragment(containerId);
                break;
            case CategoriesAdapter.GYROSCOPE_CATEGORY:
                setupGyroscopeFragment(containerId);
                break;
            case CategoriesAdapter.DEFAULTS_BUTTON_CATEGORY:
                showResetDialog();
                break;
            case CategoriesAdapter.MARKETING_BUTTON_CATEGORY:
                showMarket();
                break;
        }
    }

    private void setupColorFragment(int container) {

        if(colorFragment == null)
            colorFragment = new ColorSettingsFragment();

        currentFragment = colorFragment;
        makeTransaction(container, colorFragment);

    }

    private void setupMeshFragment(int container) {

        if(meshFragment == null)
            meshFragment = new MeshSettingsFragment();

        currentFragment = meshFragment;
        makeTransaction(container, meshFragment);

    }

    private void setupGyroscopeFragment(int container) {

        if(gyroscopeFragment == null)
            gyroscopeFragment = new GyroscopeSettingsFragment();

        currentFragment = gyroscopeFragment;
        makeTransaction(container, gyroscopeFragment);

    }

    private void showResetDialog() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.defaults_dialog_title)
                .setMessage(R.string.defaults_dialog_message)
                .setPositiveButton(R.string.reset, new DialogInterface.OnClickListener() {
                    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
                    public void onClick(DialogInterface d, int id) {
                        LiveWallpaper.settings.loadDefaults();
                        // if in dual plane mode and current fragment is present
                        if(dualPlane == true && currentFragment != null) {
                            getFragmentManager().beginTransaction()
                                    .detach(currentFragment)
                                    .attach(currentFragment)
                                    .commit();
                        }
                        d.dismiss();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface d, int id) {
                        d.dismiss();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();

    }

    private void showMarket() {

        Intent market = new Intent(Intent.ACTION_VIEW);
        market.setData(Uri.parse("market://details?id=" + MeshLW.PAID_PACKAGE));

        Intent website = new Intent(Intent.ACTION_VIEW);
        website.setData(Uri.parse("https://play.google.com/store/apps/details?id=" + MeshLW.PAID_PACKAGE));

        try {
            startActivity(market);
        } catch (ActivityNotFoundException e) {
            startActivity(website);
        }
    }

    private void makeTransaction(int container, Fragment fragment) {

        getFragmentManager().popBackStack();
        getFragmentManager()
                .beginTransaction()
                .replace(container, fragment)
                .addToBackStack(null)
                .commit();

    }
}