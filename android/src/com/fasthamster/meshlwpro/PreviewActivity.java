package com.fasthamster.meshlwpro;

import android.app.Activity;
import android.app.WallpaperInfo;
import android.app.WallpaperManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;

/**
 * Created by alex on 25.02.17.
 */
public class PreviewActivity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        WallpaperManager manager = WallpaperManager.getInstance(this);
        WallpaperInfo info = manager.getWallpaperInfo();

        // wallpaper already running start settings
        if (info != null && this.getPackageName().equals(info.getPackageName().toString())) {
            startActivity(setupSettingsIntent());
        } else {
            startActivity(setupWallpaperIntent());
        }

        finish();

    }

    private Intent setupWallpaperIntent() {

        Intent intent = new Intent();

        if(Build.VERSION.SDK_INT >= 16) {

            intent.setAction(WallpaperManager.ACTION_CHANGE_LIVE_WALLPAPER);
            intent.putExtra(WallpaperManager.EXTRA_LIVE_WALLPAPER_COMPONENT,
                    new ComponentName(this, LiveWallpaper.class));

        } else {

            intent.setAction(WallpaperManager.ACTION_LIVE_WALLPAPER_CHOOSER);
            Resources res = getResources();
            String hint = res.getString(R.string.toast_instruct_lwp_list) + res.getString(R.string.app_name);
            Toast toast = Toast.makeText(this, hint, Toast.LENGTH_LONG);
            toast.show();

        }

        return intent;
    }

    private Intent setupSettingsIntent() {

        return new Intent(this, SettingsActivity.class);

    }
}
