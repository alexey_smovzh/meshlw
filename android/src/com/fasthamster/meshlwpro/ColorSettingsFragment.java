package com.fasthamster.meshlwpro;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

/*
 * Created by alex on 26.12.16.
 */


public class ColorSettingsFragment extends Fragment {

    private AlertDialog dialog;
    private LinearLayout colorMode;
    private TextView colorModeResult;
    private LinearLayout color0, color1;
    private View color0result, color1result;
    private TextView color0title;
    private CheckBox autoColor;
    private SeekBar autoColorInterval;
    private TextView autoColorTitle, autoColorIntervalTitle, autoColorIntervalResult, autoColorIntervalMin, autoColorIntervalMax;
    private CheckBox outlines;
    private SeekBar contrastSB;
    private TextView contrastResult;

    private String[] colorModes;

    public static final String CODE_ALIAS = "com.fasthamster.meshlw.CODE";
    public static final String COLOR_ALIAS = "com.fasthamster.meshlw.COLOR";
    private static final int COLOR_DIALOG_0_RESULT_CODE = 0;
    private static final int COLOR_DIALOG_1_RESULT_CODE = 1;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Load color modes array
        colorModes = getResources().getStringArray(R.array.color_mode);

        View view = inflater.inflate(R.layout.color_settings, container, false);

        colorMode = (LinearLayout)view.findViewById(R.id.ll_color_mode);
        colorModeResult = (TextView)view.findViewById(R.id.tv_color_mode_result);
        color0 = (LinearLayout)view.findViewById(R.id.ll_color0);
        color1 = (LinearLayout)view.findViewById(R.id.ll_color1);
        color0result = view.findViewById(R.id.v_color0);
        color1result = view.findViewById(R.id.v_color1);
        color0title = (TextView)view.findViewById(R.id.tv_color0);
        autoColor = (CheckBox)view.findViewById(R.id.chk_autocolor);
        autoColorInterval = (SeekBar)view.findViewById(R.id.sb_autocolor__interval);
        autoColorTitle = (TextView)view.findViewById(R.id.tv_autocolor_title);
        autoColorIntervalTitle = (TextView)view.findViewById(R.id.tv_autocolor_interval_title);
        autoColorIntervalResult = (TextView)view.findViewById(R.id.tv_autocolor_interval_result);
        autoColorIntervalMin = (TextView)view.findViewById(R.id.tv_autocolor_interval_min);
        autoColorIntervalMax = (TextView)view.findViewById(R.id.tv_autocolor_interval_max);
        outlines = (CheckBox)view.findViewById(R.id.chk_outlines);
        contrastSB = (SeekBar)view.findViewById(R.id.sb_contrast);
        contrastResult = (TextView)view.findViewById(R.id.tv_contrast_result);


        // Dialog for color mode
        // http://androidtrainningcenter.blogspot.com/2012/07/how-to-create-custom-and-default-alert.html
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.color_mode_title);
        builder.setSingleChoiceItems(colorModes, LiveWallpaper.settings.getColorMode(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface di, int position) {
                LiveWallpaper.settings.setColorMode(position);
                updateColorWidgets(position);
                // if selected random color automatically enable automatic color change
                if(position == MeshLW.COLOR_MODE_RANDOM_COLOR || position == MeshLW.COLOR_MODE_RANDOM_GRADIENT) {
                    autoColor.setChecked(true);
                } else {
                    autoColor.setChecked(false);
                }
                // Set text result
                colorModeResult.setText(colorModes[position]);

                di.dismiss();
            }
        });
        dialog = builder.create();

        // Color mode listener
        colorMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.show();
            }
        });
        // Color choose listeners
        color0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // if chosen random color on click show toast "option not available in random color mode"
                if(LiveWallpaper.settings.getColorMode() == MeshLW.COLOR_MODE_RANDOM_COLOR
                        || LiveWallpaper.settings.getColorMode() == MeshLW.COLOR_MODE_RANDOM_GRADIENT) {
                    Toast.makeText(getActivity(), R.string.toast_color_mode, Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(getActivity(), ColorDialog.class);
                    intent.putExtra(CODE_ALIAS, 0);
                    intent.putExtra(COLOR_ALIAS, LiveWallpaper.settings.getColor0());
                    startActivityForResult(intent, COLOR_DIALOG_0_RESULT_CODE);
                }
            }
        });
        color1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(LiveWallpaper.settings.getColorMode() == MeshLW.COLOR_MODE_RANDOM_COLOR
                        || LiveWallpaper.settings.getColorMode() == MeshLW.COLOR_MODE_RANDOM_GRADIENT) {
                    Toast.makeText(getActivity(), R.string.toast_color_mode, Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(getActivity(), ColorDialog.class);
                    intent.putExtra(CODE_ALIAS, 1);
                    intent.putExtra(COLOR_ALIAS, LiveWallpaper.settings.getColor1());
                    startActivityForResult(intent, COLOR_DIALOG_1_RESULT_CODE);
                }
            }
        });

        // Autochange color checkbox listener
        autoColor.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                LiveWallpaper.settings.setAutoColor(b);
                automaticColorIntervalVisible(b);
            }
        });
        // Autochange color seekbar listener
        autoColorInterval.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {

                if(progress < MeshLW.AUTO_CHANGE_COLOR_INTERVAL_MIN)
                    progress = MeshLW.AUTO_CHANGE_COLOR_INTERVAL_MIN;
                // only on user input
                if(b == true) {
                    LiveWallpaper.settings.setAutoColorInterval(progress);
                    autoColorIntervalResult.setText(convertToMinutes(progress));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });

        // Outlines listener
        outlines.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                LiveWallpaper.settings.setOutlines(b);
            }
        });

        // Contrast seekbar listener
        contrastSB.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {

                if(b == true) {
                    LiveWallpaper.settings.setContrast(progress);
                    // on min value pointer not on left side, it moved a bit right
                    // so in preferences we hold values from 0 to 9
                    // but when use it or show to user add LIGHT_Z_MIN to get correct value from 1-10 range
                    contrastResult.setText(String.valueOf(progress + MeshLW.LIGHT_Z_MIN));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });


        return view;

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case COLOR_DIALOG_0_RESULT_CODE:
                color0result.setBackgroundColor(LiveWallpaper.settings.getColor0());
                break;
            case COLOR_DIALOG_1_RESULT_CODE:
                color1result.setBackgroundColor(LiveWallpaper.settings.getColor1());
                break;
        }
    }

    @Override
    public void onStart() {

        super.onStart();
        // load widgets states
        colorModeResult.setText(colorModes[LiveWallpaper.settings.getColorMode()]);
        autoColor.setChecked(LiveWallpaper.settings.getAutoColor());
        autoColorInterval.setProgress(LiveWallpaper.settings.getAutoColorInterval());
        autoColorIntervalResult.setText(convertToMinutes(LiveWallpaper.settings.getAutoColorInterval()));
        updateColorWidgets(LiveWallpaper.settings.getColorMode());
        outlines.setChecked(LiveWallpaper.settings.getOutlines());
        contrastSB.setProgress(LiveWallpaper.settings.getContrast());
        contrastResult.setText(String.valueOf(LiveWallpaper.settings.getContrast() + MeshLW.LIGHT_Z_MIN));

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);

    }

    private String convertToMinutes(int seconds) {

        if(seconds < 60) {
            return String.format("0:%02d", seconds);
        } else {
            return String.format("%01d:%02d", seconds / 60, seconds % 60);
        }
    }

    int c0, c1;
    // Changes color widgets appropriate to chosen color mode
    private void updateColorWidgets(int mode) {

        switch (mode) {
            case MeshLW.COLOR_MODE_COLOR:
                color0title.setText(R.string.color_tv_btn);
                color0result.setBackgroundColor(LiveWallpaper.settings.getColor0());
                color1result.setBackgroundColor(Color.TRANSPARENT);
                disableEnableControls(color0, true);
                disableEnableControls(color1, false);
                automaticColorVisible(false);
                break;

            case MeshLW.COLOR_MODE_GRADIENT:
                color0title.setText(R.string.color_tv_btn_top);
                color0result.setBackgroundColor(LiveWallpaper.settings.getColor0());
                color1result.setBackgroundColor(LiveWallpaper.settings.getColor1());
                disableEnableControls(color0, true);
                disableEnableControls(color1, true);
                automaticColorVisible(false);
                break;

            case MeshLW.COLOR_MODE_RANDOM_COLOR:
                color0title.setText(R.string.color_tv_btn);
                c0 = ColorHandler.getRandomColorStatic();
                color0result.setBackgroundColor(c0);
                color1result.setBackgroundColor(Color.TRANSPARENT);
                disableEnableControls(color0, false);
                disableEnableControls(color1, false);
                LiveWallpaper.settings.setColor0(c0);
                automaticColorVisible(true);
                break;

            case MeshLW.COLOR_MODE_RANDOM_GRADIENT:
                color0title.setText(R.string.color_tv_btn_top);
                c0 = ColorHandler.getRandomColorStatic();
                c1 = ColorHandler.getRandomColorStatic(c0);
                disableEnableControls(color0, false);
                disableEnableControls(color1, false);
                color0result.setBackgroundColor(c0);
                color1result.setBackgroundColor(c1);
                LiveWallpaper.settings.setColor0(c0);
                LiveWallpaper.settings.setColor1(c1);
                automaticColorVisible(true);
                break;

        }
    }

    // Enable/disable elements responsible for automatic color change
    private void automaticColorVisible(boolean state) {
        autoColor.setEnabled(state);
        autoColorTitle.setEnabled(state);
        // if mode - gradient && auto color - enabled  = true
        // if mode - gradient && auto color - disabled = false
        // if mode - color = false
        if(state == true) {
            if(LiveWallpaper.settings.getAutoColor() == true)
                automaticColorIntervalVisible(true);
        } else {
            automaticColorIntervalVisible(state);
        }
    }

    private void automaticColorIntervalVisible(boolean state) {
        autoColorInterval.setEnabled(state);
        autoColorIntervalTitle.setEnabled(state);
        autoColorIntervalResult.setEnabled(state);
        autoColorIntervalMin.setEnabled(state);
        autoColorIntervalMax.setEnabled(state);
    }
    // from http://stackoverflow.com/questions/7068873/how-can-i-disable-all-views-inside-the-layout
    private void disableEnableControls(ViewGroup vg, boolean enable){
        for (int i = 0; i < vg.getChildCount(); i++){
            View child = vg.getChildAt(i);
            child.setEnabled(enable);
            if (child instanceof ViewGroup){
                disableEnableControls((ViewGroup)child, enable);
            }
        }
    }
}
