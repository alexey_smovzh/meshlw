package com.fasthamster.meshlwpro;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.TextView;

/*
 * Created by alex on 26.12.16.
 */

public class MeshSettingsFragment extends Fragment {

    private SeekBar cellBar;
    private TextView cellResult;
    private CheckBox autoMesh;
    private SeekBar autoMeshInterval;
    private TextView autoMeshIntervalTitle, autoMeshIntervalResult;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.mesh_settings, container, false);

        cellBar = (SeekBar)view.findViewById(R.id.cell_bar);
        cellResult = (TextView)view.findViewById(R.id.tv_cell_result);
        autoMesh = (CheckBox)view.findViewById(R.id.chk_automesh);
        autoMeshInterval = (SeekBar)view.findViewById(R.id.sb_automesh__interval);
        autoMeshIntervalTitle = (TextView)view.findViewById(R.id.tv_automesh_interval_title);
        autoMeshIntervalResult = (TextView)view.findViewById(R.id.tv_automesh_interval_result);

        // Cell size SeekBar listener
        cellBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                // with this offset possible to move seekbar over 200, so check this situation
                if(progress > MeshLW.MAX_CELL_SIZE)
                    progress = MeshLW.MAX_CELL_SIZE;

                if(b == true) {
                    LiveWallpaper.settings.setCell(progress);
                    // on min value pointer not on left side, it moved a bit right
                    // so in preferences we hold values from 0 to 170
                    // but when use it or show to user add MIN_CELL_SIZE to get correct value from 30-200 range
                    cellResult.setText(Integer.toString(progress + MeshLW.MIN_CELL_SIZE));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });

        // Auto change mesh checkbox listener
        autoMesh.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                LiveWallpaper.settings.setAutoMesh(b);
                automaticMeshIntervalVisible(b);
            }
        });

        // Auto change mesh seekbar listener
        autoMeshInterval.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {

                if(progress < MeshLW.AUTO_CHANGE_MESH_INTERVAL_MIN)
                    progress = MeshLW.AUTO_CHANGE_MESH_INTERVAL_MIN;

                if(b == true) {
                    LiveWallpaper.settings.setAutoMeshInterval(progress);
                    autoMeshIntervalResult.setText(convertToMinutes(progress));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });

        return view;

    }

    @Override
    public void onStart() {

        super.onStart();
        // State
        cellBar.setProgress(LiveWallpaper.settings.getCell());
        cellResult.setText(Integer.toString(LiveWallpaper.settings.getCell() + MeshLW.MIN_CELL_SIZE));
        autoMesh.setChecked(LiveWallpaper.settings.getAutoMesh());
        automaticMeshIntervalVisible(LiveWallpaper.settings.getAutoMesh());
        autoMeshInterval.setProgress(LiveWallpaper.settings.getAutoMeshInterval());
        autoMeshIntervalResult.setText(convertToMinutes(LiveWallpaper.settings.getAutoMeshInterval()));

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);

    }

    private String convertToMinutes(int seconds) {

        if(seconds < 60) {
            return String.format("0:%02d", seconds);
        } else {
            return String.format("%01d:%02d", seconds / 60, seconds % 60);
        }
    }

    // Enable/disable elements responsible for automatic mesh change
    private void automaticMeshIntervalVisible(boolean state) {
        autoMeshInterval.setEnabled(state);
        autoMeshIntervalTitle.setEnabled(state);
        autoMeshIntervalResult.setEnabled(state);
    }

}

